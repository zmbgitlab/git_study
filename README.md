git 使用手册
## git ssh配置：
    1.创建SSH Key : $ ssh-keygen -t rsa -C "youremail@example.com"
    2.id_rsa.pub 复制到github里的 key 文本框里
    3.ssh -T git@example.com : 测试连接
## git 安装后的配置命令:
    1.$ git config --global user.name "Your Name"
    2.$ git config --global user.email "email@example.com"
    3.$ git clone git remote add origin git@github.com:michaelliao/learngit.git :克隆仓库
    

## git 创建版本库: $ git init

## git 版本切换(时光机穿梭):
    $ git status : 查看当前仓库状态
    $ git diff readme.txt : 查看修改内容
    $ git log --pretty=oneline : 查看提交历史(3次提交历史)
    (最近的一次是append GPL,上一次是add distributed,最早的一次是wrote a readme file)
            一大串类似1094adb..的是commit id (版本号),
            一个SHA1计算出来的一个非常大的数字,用十六进制表示。
    $ git reset --hard HEAD^ : 回退上次版本(及未来某个版本)。
        用HEAD表示当前版本，也就是最新的提交1094adb...
        上一个版本就是HEAD^，上上一个版本就是HEAD^^，
        当然往上100个版本写成HEAD~100。
    
    $ git reflog : 查看命令历史,以便确定回到未来某个版本。
    
## git 管理修改:
    $ git add readme.txt : 提交文件到暂存区
    $ git commit -m 'add file' : 暂存区的修改提交, -m : 对本地提交的说明。

# git 撤销修改:
    $ git checkout -- readme.txt : 把工作区的修改撤销(丢弃工作区)
        有两种情况：
            一种是readme.txt自修改后还没被放到暂存区,撤销修改就回到和版本库一模一样的状态；
            一种是readme.txt已经添加到暂存区后，又作了修改,撤销修改就回到添加到暂存区后的状态;
            总之，就是让这个文件回到最近一次git commit或git add时的状态。
    $ git reset HEAD readme.txt : 既可以回退版本，也可以把暂存区的修改回退到工作区, HEAD表示最新版本
# git 删除文件:
    1. $ git rm test.txt : 从版本库中删除该文件
    2. $ git commit -m 'remove test.txt'
    3. $ git checkout -- test.txt : 误删恢复
        是用版本库里的版本替换工作区的版本，无论工作区是修改还是删除，都可以“一键还原”

# git 分支管理:
    1. $ git chekcout -b dev ：创建dev分支并切换
    2. $ git branch : 查看分支
    3. $ git merge dev --no-ff: 合并分支到master分支,有合并历史
    4. $ git branch -d dev : 合并完删除分支, 删除未合并的分支 用: -D

# git bug分支管理:
    1. $ git stash : 当前工作现场“储藏”
    2. $ git checkout master : 需要在master分支上修复
        2.1 $ git checkout -b issue-101 : 从master创建临时分支
        2.2 $ $ git add readme.txt
        2.3 $ git commit -m "fix bug 101"
        2.4 $ git git checkout master : 修复完成后，切换到master分支，
        2.5 $ git merge --no-ff -m "merged bug fix 101" issue-101 : 完成合并, 最后删除issue-101分支
        2.6 $ git checkout dev : 回到"储藏"工作现场
        2.7 $ git status
        2.8 $ git stash list : 查看"储藏"列表
        2.9 $ git stash pop : 恢复工作区的同时把stash内容删了,(git stash apply:恢复,git stash drop:删除)
        ### 修复bug时，我们会通过创建新的bug分支进行修复，然后合并，最后删除；
        ### 当手头工作没有完成时，先把工作现场git stash一下，然后去修复bug，修复后，再git stash pop，回到工作现场。

# git 创建标签
    1. git branch : 查看分支
    2. git checkout master : 切换到需要打标签的分支上
    3. git tag v1.0 : 打一个新标签
    4. git tag : 查看所有标签
    ### 默认标签是打在最新提交的commit上的。如果忘了打标签，比如，现在已经是周五了，但应该在周一打的标签没有打,
    ### 方法是找到历史提交的commit id，然后打上就可以了：
    5. git log --pretty=oneline --abbrev-commit : 查看历史提交的commit id
    6. git tag v0.9 f52c633
    7. git show v0.9 : 查看标签信息
    8. git tag -a v0.1 -m "version 0.1 released" 1094adb : 指定标签信息(创建带有说明的标签，用-a指定标签名，-m指定说明文字)
    9. git tag -d v0.1 : 删除本地某个标签,
    10. git push origin v1.0 : 推送某个标签到远程
    10. git push origin --tags : 推送所有标签到远程
    12. git push origin :refs/tags/v0.9 : 删除一个远程标签